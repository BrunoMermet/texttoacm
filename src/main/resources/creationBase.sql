PRAGMA foreign_keys=ON;
BEGIN TRANSACTION;
CREATE TABLE theme(
  id_theme integer primary key autoincrement,
  nom_theme text not null);
CREATE TABLE question(
  id_question integer primary key autoincrement,
  intitule text unique not null,
  nb_colonnes int not null,
  id_theme integer references theme(id_teme));
CREATE TABLE reponse(
  id_reponse integer primary key autoincrement,
  libelle_reponse text not null,
  correcte int not null,
  id_question integer references question(id_question));
DELETE FROM sqlite_sequence;
COMMIT;
