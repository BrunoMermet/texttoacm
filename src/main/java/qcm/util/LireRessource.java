package qcm.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

public class LireRessource {
  public static String lire(String nomFichier) {
    String texte = null;
    try {
      InputStream flux = LireRessource.class.getClassLoader().getResourceAsStream(nomFichier);
      BufferedReader reader = new BufferedReader(new InputStreamReader(flux));
      StringBuilder builder = new StringBuilder();
      String ligne;
      while ((ligne = reader.readLine()) != null) {
        builder.append(ligne).append('\n');
      }
      texte = builder.toString();
    } catch (Exception e) {
      System.err.println("erreur à la lecture de " + nomFichier + " : " + e.getMessage());
      System.exit(1);
    }
    return texte;
  }
  
  public static List<String> lireLignes(String nomFichier) {
    //String[] lignes = null;
    List<String> liste = new LinkedList<>();
    try {
      InputStream flux = LireRessource.class.getClassLoader().getResourceAsStream(nomFichier);
      BufferedReader reader = new BufferedReader(new InputStreamReader(flux));
      StringBuilder builder = new StringBuilder();
      String ligne;
      while ((ligne = reader.readLine()) != null) {
        liste.add(ligne);
      }
      //lignes = liste.toArray(n -> new String[n]);
    } catch (Exception e) {
      System.err.println("erreur à la lecture de " + nomFichier + " : " + e.getMessage());
      System.exit(1);
    }
    return liste;
  }
}
