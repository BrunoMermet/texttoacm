package qcm.parsing;

import qcm.data.GestionnaireConnexion;

public class Parser {

  public static void main(String[] args) {
    GestionnaireConnexion.getInstance("qcm.db");
    ParserFichier parser = new ParserFichier("questions.txt");
    parser.execute();
    
  }

}
