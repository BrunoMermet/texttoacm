package qcm.parsing;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Optional;
import java.util.stream.Stream;

import qcm.data.GestionnaireConnexion;
import qcm.data.Question;

public class ParserFichier {
  private String fichier;
  private Path cheminFichier;
  private Optional<Question> questionCourante;
  private Connection connexion;
  private boolean enonceEnCours;
  private int numLigne;
  
  public ParserFichier(String leFichier) {
    fichier = leFichier;
    cheminFichier = Paths.get(fichier);
    enonceEnCours = false;
    numLigne = 0;
  }
  
  public void execute() {
    connexion = GestionnaireConnexion.getConnexion();
    Stream<String> contenu = null;
    try {
      contenu = Files.lines(cheminFichier);
    } catch (IOException e) {
      System.err.println("Erreur à la lecture du fichier : " + e.getMessage());
      System.exit(1);
    }
    
    questionCourante = Optional.empty();
    
    contenu.forEach(ligne -> traiteLigne(ligne));
    questionCourante.ifPresent(q -> ajouterALaBase(q));
    
  }
  
  private void traiteLigne(String ligne) {
    numLigne++;
    if (ligne.startsWith("Q: ")) {
      questionCourante.ifPresent(q -> ajouterALaBase(q));
      questionCourante = Optional.of(new Question(ligne.substring(3)));
      enonceEnCours = true;
    } else if (ligne.startsWith("B: ")) {
      questionCourante.ifPresent(q -> q.addBonneReponse(ligne.substring(3)));
      enonceEnCours = false;
    } else if (ligne.startsWith("M: ")) {
      questionCourante.ifPresent(q -> q.addMauvaiseReponse(ligne.substring(3)));
      enonceEnCours = false;
    } else if (ligne.startsWith("T: ")) {
      questionCourante.ifPresent(q -> q.setTheme(ligne.substring(3)));
      enonceEnCours = false;
    } else if (ligne.startsWith("C: ")) {
        questionCourante.ifPresent(q -> q.setNbColonnes(Integer.parseInt(ligne.substring(3))));
        enonceEnCours = false;
    } else if (! ligne.isBlank() && !ligne.startsWith("#")) {
      if (enonceEnCours) {
        questionCourante.ifPresent(q -> q.completer(ligne));
      } else {
        System.err.println("Erreur : ligne " + numLigne + " non reconnue : " + ligne);
      }
    }
  }
  
  private void ajouterALaBase(Question q) {
    System.out.println("Ajout de la question : \n" + q);
    q.ajouterALaBase(connexion);
  }

}
