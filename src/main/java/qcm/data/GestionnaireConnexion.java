package qcm.data;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class GestionnaireConnexion {
  private static Connection connexion;
  private String baseDeDonnees;
  private static GestionnaireConnexion instance = null;
  
  private GestionnaireConnexion(String laBaseDeDonnees) {
    baseDeDonnees = laBaseDeDonnees;
    connexionBaseDeDonnees();
  }
  
  public static GestionnaireConnexion getInstance(String laBaseDeDonnees) {
    if (instance == null) {
      instance = new GestionnaireConnexion(laBaseDeDonnees);
    }
    return instance;
  }
  
  private void connexionBaseDeDonnees() {
    System.out.print("Chargement du driver sqlite3... ");
    try {
      Class.forName("org.sqlite.JDBC"); // Charger le pilote JDBC SQLite
      System.out.println("réussi");
    } catch (ClassNotFoundException e) {
      System.err.println("échoué : " + e.getMessage());
      System.exit(1);
    }
    System.out.print("Connexion à la base de données... ");
    try {
      connexion = DriverManager.getConnection("jdbc:sqlite:" + baseDeDonnees);
      System.out.println("réussie");
    } catch (Exception e) {
      System.err.println("échouée : " + e.getMessage());
      System.exit(1);
    }
  }
  
  public static Connection getConnexion() {
    return connexion;
  }
  
  public static void deconnecter() {
    System.out.print("Déconnexion de la base de données... ");
    try {
      connexion.close();
      System.out.println("réussie");
    } catch (Exception e) {
      System.err.println("échouée : " + e.getMessage());
      System.exit(1);
    }
  }
}
