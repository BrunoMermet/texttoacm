package qcm.data;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import qcm.util.LireRessource;

public class QCM {
  private List<Question> questions;
  private Map<String, Integer> mapQuestions;
  private String titre;
  private String sousTitre;
  private String date;
  private List<String> consignes;
  private Base base;
  
  
  
  public QCM(Base laBase) {
    questions = new ArrayList<>();
    mapQuestions = new HashMap<>();
    consignes = new ArrayList<>();
    base = laBase;
  }
  
  public void ajouterTheme(String theme, int nbQuestions) {
      mapQuestions.put(theme, nbQuestions);
  }
  
  public void setTitre(String titre) {
    this.titre = titre;
  }
  
  public void setSousTitre(String sousTitre) {
    this.sousTitre = sousTitre;
  }
  
  public void setDate(String date) {
    this.date = date;
  }
  
  public void addConsigne(String consigne) {
    consignes.add(consigne);
  }
  
  private void remplirQuestions() {
    questions = base.extraireQuestions(mapQuestions);
  }
  
  public void generer(String sortie) {
    try {
    Path cheminSortie = Paths.get(sortie);
    String fichier1 = this.getClass().getClassLoader()
        .getResource("partie1.tex").getPath();
    Path pathFichier1 = Paths.get(fichier1);
    String fichier2 = this.getClass().getClassLoader()
        .getResource("partie2.tex").getPath();
    Path pathFichier2 = Paths.get(fichier2);
    String fichier3 = this.getClass().getClassLoader()
        .getResource("partie3.tex").getPath();
    Path pathFichier3 = Paths.get(fichier3);
    String fichier4 = this.getClass().getClassLoader()
        .getResource("partie4.tex").getPath();
    Path pathFichier4 = Paths.get(fichier4);
    remplirQuestions();
    Files.write(cheminSortie, LireRessource.lireLignes("partie1.tex"),
        Charset.forName("UTF-8"), StandardOpenOption.CREATE_NEW);
    Files.write(cheminSortie, questions.stream()
                                       .map(Question::toLatex)
                                       .toList(),
        Charset.forName("UTF-8"), StandardOpenOption.APPEND);
    Files.write(cheminSortie, mapQuestions.keySet()
                                          .stream()
                                          .map(theme -> "\\copygroup{" + theme + "}{tout}"
                                              + "").toList(),
        Charset.forName("UTF-8"), StandardOpenOption.APPEND);
    Files.write(cheminSortie, LireRessource.lireLignes("partie2.tex"),
        Charset.forName("UTF-8"), StandardOpenOption.APPEND);
    List<String> entete = genererEntete();
    Files.write(cheminSortie, entete,
        Charset.forName("UTF-8"), StandardOpenOption.APPEND);
    Files.write(cheminSortie, LireRessource.lireLignes("partie3.tex"),
        Charset.forName("UTF-8"), StandardOpenOption.APPEND);
    Files.write(cheminSortie, consignes.stream()
                                       .map(cons -> "\\item " + cons)
                                       .toList(),
                  Charset.forName("UTF-8"), StandardOpenOption.APPEND);
    Files.write(cheminSortie, LireRessource.lireLignes("partie4.tex"),
        Charset.forName("UTF-8"), StandardOpenOption.APPEND);
    } catch (Exception e) {
      System.err.println("type : " + e.getClass().getName());
      System.err.println("Erreur à l'écriture du fichier : " + e.getMessage());
      System.exit(1);
    }
  }
  
  private List<String> genererEntete() {
    List<String> entete = new ArrayList<>();
    entete.add("\\vspace{.3cm}");
    entete.add("{\\huge " + titre + "}\\\\");
    entete.add("\\vspace{.3cm}");
    entete.add("{\\Large " + sousTitre + "}\\\\");
    entete.add("\\vspace{.1cm}");
    entete.add("{\\large " + date + "}\\\\");
    entete.add("\\vspace{.3cm}");
    return entete;
  }
}
