package qcm.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Base {
  private Map<String, List<Question>> contenu;
  private Connection connexion;
  
  public Base() {
    contenu = new HashMap<>();
  }
  
  public void remplir() {
    connexion = GestionnaireConnexion.getConnexion();
    completer();
    GestionnaireConnexion.deconnecter();
  }
  
  private List<Question> extraireElementsAleatoirement(List<Question> questions, int nbElements) {
    List<Question> selectionQuestions = new ArrayList<>();
    for (int i = 0; i < nbElements; i++) {
      int index = (int) (Math.random() * questions.size());
      selectionQuestions.add(questions.get(index));
      questions.remove(index);
    }
    return selectionQuestions;
  }
  
  public List<Question> extraireQuestions(Map<String, Integer> selection) {
    List<Question> selectionQuestions = new ArrayList<>();
    selection.forEach((theme, nbQuestions) -> {
      List<Question> questions = contenu.get(theme);
      if (questions == null) {
        System.err.println("Thème inconnu : " + theme);
      } else if (nbQuestions > questions.size()) {
        System.err.println("Pas assez de questions pour le thème " + theme);
        selectionQuestions.addAll(questions);
      } else {
        selectionQuestions.addAll(extraireElementsAleatoirement(questions, nbQuestions));
      }
    });
    return selectionQuestions;
  }
  
  private void chargementThemes() {
    System.out.print("Chargement des thèmes... ");
    try {
      PreparedStatement getThemes = connexion.prepareStatement("SELECT id_theme, nom_theme FROM theme ORDER BY nom_theme");
      ResultSet themes = getThemes.executeQuery();
      while (themes.next()) {
           contenu.put(themes.getString(2), new ArrayList<Question>());
        }
      System.out.println("réussi");
    } catch (Exception e) {
      System.err.println("échoué : " + e.getMessage());
      System.exit(1);
    }
  
  }

  private void chargementQuestions() {
    System.out.print("Chargement des questions... ");
    try {
      PreparedStatement getQuestions = connexion
          .prepareStatement("SELECT id_question, nom_theme FROM question JOIN theme ON question.id_theme = theme.id_theme ORDER BY id_question");
      ResultSet questions = getQuestions.executeQuery();
      while (questions.next()) {
        Question question = Question.charger(questions.getInt(1));
        contenu.get(questions.getString(2)).add(question);
      }
      System.out.println("réussi");
    } catch (Exception e) {
      System.err.println("échoué : " + e.getMessage());
      System.exit(1);
    }
  }
  
  private void completer() {
    chargementThemes();
    chargementQuestions();
  }
 
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    contenu.forEach((theme, questions) -> {
      sb.append(theme)
        .append(" :\n")
        .append("-".repeat(theme.length()+2))
        .append("\n");
      questions.forEach(q -> sb.append(q.toString()).append("\n"));
    });
    return sb.toString();
  }
}
