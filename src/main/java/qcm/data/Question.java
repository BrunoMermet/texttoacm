package qcm.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class Question {
  private String intitule;
  private List<String> bonnesReponses;
  private List<String> mauvaisesReponses;
  private String theme;
  private int idTheme;
  private int idQuestion;
  private int nbColonnes;
  
  public Question(String lIntitule) {
    this.intitule = lIntitule;
    bonnesReponses = new LinkedList<>();
    mauvaisesReponses = new LinkedList<>();
    nbColonnes = 1;
  }
  
  public void completer(String laSuite) {
    intitule += "\n" + laSuite;
  }
  
  public void addBonneReponse(String laReponse) {
    bonnesReponses.add(laReponse);
  }
  
  public void addMauvaiseReponse(String laReponse) {
    mauvaisesReponses.add(laReponse);
  }
  
  public void setTheme(String leTheme) {
    theme = leTheme;
  }
  
  public void setNbColonnes(int nb) {
    nbColonnes = nb;
  }
  
  public void ajouterALaBase(Connection connexion) {
    ajouterThemeSiNecessaire(connexion);
    ajouterQuestion(connexion);
    ajouterBonnesReponses(connexion);
    ajouterMauvaisesReponses(connexion);
  }
  
  private void ajouterThemeSiNecessaire(Connection connexion) {
    try {
      PreparedStatement getThemes = connexion.prepareStatement("SELECT id_theme, nom_theme FROM theme ORDER BY nom_theme");
      ResultSet themes = getThemes.executeQuery();
      System.err.println("récupération des thèmes");
      while (themes.next()) {
        if (themes.getString(2).equals(theme)) {
          idTheme = themes.getInt(1);
          return;
        }
      }
      System.err.println("ajout thème");
      PreparedStatement ajoutTheme = connexion.prepareStatement("INSERT INTO theme (nom_theme) VALUES (?)");
      ajoutTheme.setString(1, theme);
      ajoutTheme.execute();
      System.err.println("récupération id thème");
      PreparedStatement getIdTheme = connexion.prepareStatement("SELECT id_theme FROM theme WHERE nom_theme = ?");
      getIdTheme.setString(1, theme);
      ResultSet requeteID = getIdTheme.executeQuery();
      requeteID.next();
      idTheme = requeteID.getInt(1);
    } catch (SQLException e) {
      System.err.println("Erreur SQL pour traitement thème : " + e.getMessage());
      System.exit(1);
    }
  }
  
  private void ajouterQuestion(Connection connexion) {
    try {
      PreparedStatement ajoutQuestion = connexion
          .prepareStatement("INSERT INTO question (intitule, id_theme, nb_colonnes) VALUES (?, ?, ?)");
      ajoutQuestion.setString(1, intitule);
      ajoutQuestion.setInt(2, idTheme);
      ajoutQuestion.setInt(3, nbColonnes);
      ajoutQuestion.execute();
      PreparedStatement getIdQuestion = connexion
          .prepareStatement("SELECT id_question FROM question WHERE intitule = ?");
      getIdQuestion.setString(1, intitule);
      ResultSet requeteID = getIdQuestion.executeQuery();
      requeteID.next();
      idQuestion = requeteID.getInt(1);
    } catch (SQLException e) {
      System.err.println("Erreur SQL pour traitement question : " + e.getMessage());
      System.exit(1);
    }
  }
  
  private void ajouterBonnesReponses(Connection connexion) {
    try {
      PreparedStatement ajoutReponse = connexion
          .prepareStatement("INSERT INTO reponse (id_question, libelle_reponse, correcte) VALUES (?, ?, 1)");
      for (String reponse : bonnesReponses) {
        ajoutReponse.setInt(1, idQuestion);
        ajoutReponse.setString(2, reponse);
        ajoutReponse.execute();
      }
    } catch (SQLException e) {
      System.err.println("Erreur SQL pour traitement bonne réponse : " + e.getMessage());
      System.exit(1);
    }
  }
  
  private void ajouterMauvaisesReponses(Connection connexion) {
    try {
      PreparedStatement ajoutReponse = connexion
          .prepareStatement("INSERT INTO reponse (id_question, libelle_reponse, correcte) VALUES (?, ?, 0)");
      for (String reponse : mauvaisesReponses) {
        ajoutReponse.setInt(1, idQuestion);
        ajoutReponse.setString(2, reponse);
        ajoutReponse.execute();
      }
    } catch (SQLException e) {
      System.err.println("Erreur SQL pour traitement mauvaise réponse : " + e.getMessage());
      System.exit(1);
    }
  }
  
  public String toLatex() {
    if (bonnesReponses.size() == 1) {
      return toLatexUnique();
    } else {
      return toLatexMultiple();
    }
  }
  
  private void chargerReponses() {
    Connection connexion = GestionnaireConnexion.getConnexion();
    try {
      PreparedStatement reqReponses = connexion
          .prepareStatement("SELECT libelle_reponse, correcte FROM reponse WHERE id_question = ?");
      reqReponses.setInt(1, idQuestion);
      ResultSet resReponses = reqReponses.executeQuery();
      while (resReponses.next()) {
        if (resReponses.getInt(2) == 1) {
          bonnesReponses.add(resReponses.getString(1));
        } else {
          mauvaisesReponses.add(resReponses.getString(1));
        }
      }
    } catch (SQLException e) {
      System.err.println("Erreur SQL pour chargement réponses : " + e.getMessage());
      System.exit(1);
    }
  }
  
  public static Question charger(int id) {
    Question question = null;
    try {
      Connection connexion = GestionnaireConnexion.getConnexion();
      PreparedStatement reqQuestion = connexion
        .prepareStatement("SELECT intitule, question.id_theme, nom_theme, nb_colonnes FROM question JOIN theme ON question.id_theme = theme.id_theme WHERE id_question = ?");
      reqQuestion.setInt(1, id);
      ResultSet resQuestion = reqQuestion.executeQuery();
      resQuestion.next();
      question = new Question(resQuestion.getString(1));
      question.idQuestion = id;
      question.idTheme = resQuestion.getInt(2);
      question.theme = resQuestion.getString(3);
      question.nbColonnes = resQuestion.getInt(4);
      question.chargerReponses();
    } catch (SQLException e) {
      System.err.println("Erreur SQL pour chargement question : " + e.getMessage());
      System.exit(1);
    }
    return question;
  }
  
  private String toLatexUnique() {
    StringBuilder sb = new StringBuilder();
    sb.append("\\element{" + theme + "}{\n");
    sb.append("  \\begin{question}{" + theme + ":" + idQuestion + "}");
    sb.append("\\bareme{b=1}\n");
    sb.append(intitule).append("\n");
    if (nbColonnes > 1) {
      sb.append("    \\begin{multicols}{" + nbColonnes + "}\n");
    }
    sb.append("    \\begin{reponses}\n");
    sb.append("      \\bonne{" + bonnesReponses.get(0) + "}\n");
    for (String reponse : mauvaisesReponses) {
      sb.append("    \\mauvaise{").append(reponse).append("}\n");
    }
    sb.append("    \\end{reponses}\n");
    if (nbColonnes > 1) {
      sb.append("    \\end{multicols}\n");
    }
    sb.append("  \\end{question}\n");
    sb.append("}\n");
    return sb.toString();
  }

  private String toLatexMultiple() {
    StringBuilder sb = new StringBuilder();
    sb.append("\\element{" + theme + "}{\n");
    sb.append("  \\begin{questionmult}{" + theme + ":" + idQuestion + "}\n");
    sb.append(intitule).append("\n");
    if (nbColonnes > 1) {
      sb.append("    \\begin{multicols}{" + nbColonnes + "}\n");
    }
    sb.append("    \\begin{reponses}\n");
    for (String reponse : bonnesReponses) {
      sb.append("      \\bonne{").append(reponse).append("}");
      sb.append("\\bareme{b=1/"+bonnesReponses.size()+",");
      sb.append("m=0}\n");
    }
    for (String reponse : mauvaisesReponses) {
      sb.append("      \\mauvaise{").append(reponse).append("}");
      sb.append("\\bareme{b=0,");
      sb.append("m=-1/"+mauvaisesReponses.size()+"}\n");       
    }
    sb.append("    \\end{reponses}\n");
    if (nbColonnes > 1) {
      sb.append("    \\end{multicols}\n");
    }
    sb.append("  \\end{questionmult}\n");
    sb.append("}\n");
    return sb.toString();
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(intitule).append("\n");
    bonnesReponses.forEach(reponse -> sb.append(reponse).append(" (+)\n"));
    mauvaisesReponses.forEach(reponse -> sb.append(reponse).append(" (-)\n"));
    return sb.toString();
  }
}
