package qcm;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import qcm.data.Base;
import qcm.data.GestionnaireConnexion;
import qcm.data.QCM;
import qcm.init.InitialiserBDD;
import qcm.parsing.ParserFichier;

/**
 * Hello world!
 *
 */
public class Main {
  public static void afficherAide() {
    System.out.println("Usage: java -jar qcm.jar <command>");
    System.out.println("Commands:");
    System.out.println("  init: initialise la base de données");
    System.out.println("  parse: ajoute des questions à la base de données à partir d'un fichier");
    System.out.println("  print: affiche les questions de la base de données");
    System.out.println("  generate: produit un fichier latex pour AMC");
    System.out.println("  help <command>: affiche l'aide pour la commande spécifiée");
    System.exit(0);    
  }
  
  public static void afficherAideInitialiser() {
    System.out.println("Usage: java -jar qcm.jar init <baseDeDonnees>");
    System.out.println("Crée et Initialise la base de données");
    System.out.println("Il s'agit d'un fichier sqlite3 suffixé par .db");
  }

  public static void afficherAideParser() {
    System.out.println("Usage: java -jar qcm.jar parse <baseDeDonnees> <fichier>");
    System.out.println("Parse un fichier de questions et les ajoute à la base de données");
    System.out.println();
    System.out.println("Format du fichier :");
    System.out.println("Q: énoncé de la question");
    System.out.println("   L'énoncé peut être sur plusieurs lignes.");
    System.out.println("   sans remettre 'Q: ' au début des autres lignes");
    System.out.println("B: réponse bonne");
    System.out.println("M: réponse mauvaise");
    System.out.println("T: thème de la question");
    System.out.println("C: nombre de colonnes pour les réponses");
    System.out.println("   faculatif. Vaut 1 par défaut.");
  }
  
  public static void afficherAideAfficher() {
    System.out.println("Usage: java -jar qcm.jar print <baseDeDonnees>");
    System.out.println("Affiche les questions de la base de données");
  }

  public static void afficherAideGenerer() {
    System.out.println("Usage: java -jar qcm.jar generate <baseDeDonnees> <description> <sortie>");
    System.out.println("Crée un QCM en latex pour auto-multiple-choice");
    System.out.println("BaseDeDonnees est le fichier sqlite3 contenant les questions");
    System.out.println("Description est un fichier décrivant le contenu du QCM");
    System.out.println("Sortie est le fichier de sortie");
    System.out.println();
    System.out.println("Format du fichier de description :");
    System.out.println("titre: titre du QCM");
    System.out.println("sous-titre: sous-titre du QCM");
    System.out.println("date: date du QCM");
    System.out.println("consigne: consigne du QCM");
    System.out.println("          une consigne par ligne");
    System.out.println("theme: nom du thème, nombre de questions à extraire");
    System.out.println("       un thème par ligne");
  }

  public static void initialiser(String[] args) {
    if (args.length != 2) {
      afficherAideInitialiser();
      System.exit(1);
    }
    if (Files.exists(Paths.get(args[1]))) {
      System.err.println("La base de données existe déjà");
      System.exit(1);
    }
    GestionnaireConnexion.getInstance(args[1]);
    InitialiserBDD initialiser = new InitialiserBDD();
    initialiser.execute();
    GestionnaireConnexion.deconnecter();
  }
  
  public static void parser(String[] args) {
    if (args.length != 3) {
      afficherAideParser();
      System.exit(1);
    }
    GestionnaireConnexion.getInstance(args[1]);
    ParserFichier parser = new ParserFichier(args[2]);
    parser.execute();
    GestionnaireConnexion.deconnecter();
  }
  
  public static void afficher(String[] args) {
    if (args.length != 2) {
      afficherAideAfficher();
      System.exit(1);
    }
    GestionnaireConnexion.getInstance(args[1]);
    Base base = new Base();
    base.remplir();
    System.out.println(base);
    GestionnaireConnexion.deconnecter();
  }
  
  private static void traiteDescription(QCM qcm, List<String> description) {
    for (String ligne : description) {
      String[] elements = ligne.split(":", 2);
      switch (elements[0]) {
      case "titre" -> qcm.setTitre(elements[1]);
      case "sous-titre" -> qcm.setSousTitre(elements[1]);
      case "date" -> qcm.setDate(elements[1]);
      case "consigne" -> qcm.addConsigne(elements[1]);
      case "theme" -> {
        String[] theme = elements[1].split(",");
        qcm.ajouterTheme(theme[0].trim(), Integer.parseInt(theme[1].trim()));
        }
      }
    }
  }
  
  public static void exploiter(String[] args) {
    if (args.length != 4) {
      afficherAideAfficher();
      System.exit(1);
    }
    GestionnaireConnexion.getInstance(args[1]);
    Base base = new Base();
    base.remplir();
    QCM qcm = new QCM(base);
    Path cheminDescription = Paths.get(args[2]);
    List<String> description = null;
    try {
      description = Files.readAllLines(cheminDescription);
    } catch (Exception e) {
      System.err.println("Erreur à la lecture du fichier de description : " + e.getMessage());
      System.exit(1);
    }
    traiteDescription(qcm, description);
    qcm.generer(args[3]);
    GestionnaireConnexion.deconnecter();
  }
  
  public static void aider(String[] args) {
    if (args.length != 2) {
      afficherAide();
      System.exit(1);
    }
    switch (args[1]) {
    case "init" -> afficherAideInitialiser();
    case "parse" -> afficherAideParser();
    case "print" -> afficherAideAfficher();
    case "generate" -> afficherAideGenerer();
    default -> afficherAide();
    }
  }
  
    public static void main( String[] args ) {
      if (args.length == 0) {
        afficherAide();
      } else {
        switch (args[0]) {
        case "init" -> initialiser(args);
        case "parse"-> parser(args);
        case "print" -> afficher(args);
        case "generate" -> exploiter(args);
        case "help" -> aider(args);
        default -> afficherAide();
        }
      }
    }
}
