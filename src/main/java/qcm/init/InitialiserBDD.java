package qcm.init;

import java.sql.Connection;
import java.sql.Statement;

import qcm.data.GestionnaireConnexion;
import qcm.util.LireRessource;

public class InitialiserBDD {
  private Connection connexion;
    
  public void execute() {
    connexion = GestionnaireConnexion.getConnexion();
    initialisation();
    GestionnaireConnexion.deconnecter();
  }
    
  private void initialisation() {
    System.out.print("Initialisation de la base de données... ");
    Statement statement = null;
    try {
      statement = connexion.createStatement();
      String requete = LireRessource.lire("creationBase.sql");
      statement.executeUpdate(requete);
      statement.close();
    } catch (Exception e) {
      System.err.println("exception : " + e.getClass().getName());
      System.err.println("échouée : " + e.getMessage());
      System.exit(1);
    }
  }
}
