package qcm.init;

import qcm.data.GestionnaireConnexion;

public class Initialiser {
  public static void main(String[] args) {
    GestionnaireConnexion.getInstance("qcm.db");
    InitialiserBDD initialiser = new InitialiserBDD();
    initialiser.execute();
  }
}
