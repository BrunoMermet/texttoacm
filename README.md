# Text to Auto-multiple-choice

Logiciel permettant de créer facilement un qcm auto-multiple-choice
à partir de questions au format texte.

Les questions sont saisies dans un ou plusieurs fichiers texte. Elles
sont ensuite intégrées à une base de données sqlite3. Enfin, un QCM
utilisant ces questions peut être généré à partir d'un fichier de
configuration.

## Format du fichier de questions
Dans ce fichier, les questions et leurs réponses sont mises les unes
à la suite des autres. Les lignes vides et celles commençant par un #
sont ignorées.

Une question s'écrit ainsi :
```
Q: l'énoncé de la question.
Celui-ci peut s'étendre sur plusieurs lignes ainsi.
T: le thème de la question (une ligne)
B: une bonne réponse
B: il peut y avoir plusieurs bonnes réponses
M: une mauvaise réponse
M: encore une mauvaise réponse
M: et toujours une mauvaise réponse
C: nombre de colonnes pour les réponses (facultatif, 1 par défaut)
```

## Format du fichier de configuration d'un QCM
La configuration d'un QCM se fait également au moyen d'un fichier texte.
Celui-ci a le format suivant :
```
titre: Titre du QCM
sous-titre: Sous-titre du QCM
date: Date du QCM
consigne: une consigne pour les élèves
consigne: il peut y avoir plusieurs consignes
thème: nom du thème, nombre de questions
thème: autre thème, nombre de questions
```

## Utilisation du programme
Le programme se présente sous la forme d'un fichier `jar`. Celui-ci
s'utilise ainsi :

`java -jar qcm.jar` *commande* *paramètres*

Le programme intègre une aide pour son utilisation (commande `help`).

Les différentes commandes disponibles sont les suivante :

- `init` <baseDeDonnées> : crée une nouvelle base de données. Le fichier spécifié **ne doit pas exister** (sécurité pour éviter d'écraser une base existante).

- `parse` <baseDeDonnées> <questions> : ajoute les questions présentes dans e ficher de question à la base de données. Ne vérifie pas l'existence des questions avant de les ajouter.

- `print` <baseDeDonnées> : permet d'afficher la liste des questions, par thèmes, stockées dans la base de données.

- `generate` <baseDeDonnées> <configuration> <sortie> : génère dans le fichier <sortie> le latex compatible auto-multiple-choice pour le qcm configuré dans <cofiguration> à partir de la base de données.

- `help` <commande> : affiche l'aide pour la commande donnée.
